package idv.kevin.springSecurity.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MyAuthenticationSuccesHandler implements AuthenticationSuccessHandler {

    //jackson工具類,將物件轉換為json字串
    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * @param httpServletRequest
     * @param httpServletResponse
     * @param authentication      驗證成功後取得的訊息
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        //回傳json字串回前端
        Map result = new HashMap();
        result.put("success", true);

        String json = objectMapper.writeValueAsString(result);
        httpServletResponse.setContentType("text/json;charset=utf-8");
        httpServletResponse.getWriter().write(json);
    }
}
